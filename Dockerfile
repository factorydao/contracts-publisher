FROM node:14
RUN apt-get -y update && apt-get install -y git
RUN npm install -g truffle
WORKDIR /usr/src/app
COPY package*.json index.js ./
RUN npm ci
ENTRYPOINT ["npm", "start"]