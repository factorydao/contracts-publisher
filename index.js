const { exec } = require('child_process');
const { stdout, stderr } = require('process');
const fs = require('fs');
require('dotenv').config();
var app = require('express')();
var http = require('http').Server(app);
var contractsPath = __dirname + '/contracts-repo/build/contracts/';
var repoExists = function(){
    return fs.existsSync(__dirname + '/contracts-repo');
}

app.get('/download/:contractName', function(req,res){
    if(!req.params.contractName)
        res.status(400).send('Missing path param');
    else
        res.download(contractsPath + req.params.contractName);
});

app.post('/update', function(req, res){
    if(!req.query.auth || !req.query.auth === process.env.AUTH_KEY){
        return res.status(500).send({ result: 'failure', error: 'Wrong key' });
    }
    if(repoExists()){
        updateContracts();
    } else {
        cloneRepo();
    }
    res.send({ result: 'success', error: 'Update in progress' });
});

http.listen(8000, function(){
    console.log('Server is live on ' + 8000);
});


const cloneRepo = async function(){
    exec(`git clone ${process.env.CONTRACT_REPO} contracts-repo && cd contracts-repo && npm install && truffle deploy`, (error, stdout, stderr) => {
        if (error) {
            console.log(`error: ${error.message}`);
            return;
        }
        if (stderr) {
            console.log(`stderr: ${stderr}`);
            return;
        }
        console.log(`stdout: ${stdout}`);
        return;
    });
};

const updateContracts = async function(){
    exec("cd contracts-repo && git pull && npm install && truffle deploy", (error, stdout, stderr) => {
        if (error) {
            console.log(`error: ${error.message}`);
            return;
        }
        if (stderr) {
            console.log(`stderr: ${stderr}`);
            return;
        }
        console.log(`stdout: ${stdout}`);
        return;
    });
};